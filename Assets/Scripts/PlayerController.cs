using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private CharacterController _controller;

    [SerializeField]
    private float _speed = 10f;

    Vector3 velocity;
    bool isGrounded;

    [SerializeField]
    private float _gravity = -9.81f;

    [SerializeField]
    private Transform _groundCheck;

    [SerializeField]
    private float _groundDistance = 0.4f;

    [SerializeField]
    private LayerMask _groundMask;


    void Start()
    {
        
    }

    
    void Update()
    {
        isGrounded = Physics.CheckSphere(_groundCheck.position, _groundDistance, _groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        _controller.Move(move * _speed * Time.deltaTime);

        velocity.y += _gravity * Time.deltaTime;

        _controller.Move(velocity * Time.deltaTime);
    }

    
}
