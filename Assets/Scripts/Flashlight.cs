using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
   private Light _light;
   private bool _isLightON;

   private void Start()
   {
       _light = GetComponent<Light>();
       _isLightON = false;
       _light.enabled = false;
   }

   private void Update()
   {
       if (Input.GetKeyDown(KeyCode.F) && Gamedata._hasCable == true)
       {
           _light.enabled = !_light.enabled;
           //ToggleLight();
       }
   }

    /*private void ToggleLight()
    {
        if(_isLightON)
        {
            _light.enabled = true;
        }
        else
        {
            _light.enabled = false;
        }
    }*/
}
