using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    [SerializeField]
    private float _mouseSens = 100f;
    
    [SerializeField]
    private Transform _playerLook;

    float xRotation = 0f;

    void Start()
    {
       Cursor.lockState = CursorLockMode.Locked;
       
    }

    
    void Update()
    {
      float mouseX = Input.GetAxis("Mouse X") * _mouseSens * Time.deltaTime;
      float mouseY = Input.GetAxis("Mouse Y") * _mouseSens * Time.deltaTime;

      xRotation -= mouseY;
      xRotation = Mathf.Clamp(xRotation, -90f, 90f);

      transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
      _playerLook.Rotate(Vector3.up * mouseX);
    }
}
