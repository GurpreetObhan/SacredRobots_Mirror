using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    private Camera _camera;

    [SerializeField]
    private GameObject _pickupMessage;

    private bool _canSeePickup;

    private void Start()
    {
        _camera = Camera.main;
        _pickupMessage.SetActive(false);
    }

    private void Update()
    {
        var nearestGameObject = GetNearestGameObject();
        //var movableObjects = GetMovableObject();
        if (nearestGameObject == null) return;

        var ray = _camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit, 20))
        {
            if (hit.transform.tag == "Movable")
            {
                _canSeePickup = true;
            }
            else
            {
                _canSeePickup = false;
            }
        }

        if (_canSeePickup == false)
        {
            _pickupMessage.SetActive(false);
        }

        if (_canSeePickup == true)
        {
            _pickupMessage.SetActive(true);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            var interactable = nearestGameObject.GetComponent<IInteractable>();
            interactable?.Interact();
        }

        /*if (movableObjects == null) return;
        if (Input.GetKey(KeyCode.Mouse0))
        {
            var interactable = movableObjects.GetComponent<IInteractable>();
            interactable?.Interact();
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                interactable = movableObjects.GetComponent<IInteractable>();
                interactable?.DontInteract();
            }
        }*/


    }

    private GameObject GetNearestGameObject()
    {
        GameObject result = null;
        var ray = _camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit, 5))
        {
            result = hit.transform.gameObject;
        }
        return result;
    }

    /*private GameObject GetMovableObject()
    {
        GameObject result = null;
        var ray = _camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit, 3))
        {
            if (hit.transform.gameObject.tag == "Movable")
            {
                result = hit.transform.gameObject;
            }
        }
        return result;
    }*/
}
