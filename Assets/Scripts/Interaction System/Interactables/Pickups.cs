using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickups : MonoBehaviour, IInteractable
{
    [SerializeField]
    private Transform _hands;

    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Interact()
    {
        _rb.useGravity = false;
        this.transform.position = _hands.position;

    }

    public void DontInteract()
    {
        _rb.useGravity = false;
    }
}
