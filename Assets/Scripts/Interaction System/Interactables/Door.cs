using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    private Animator _anim;
    
    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    
    public void Interact()
    {
       //_anim.SetBool("isOpen", true);
       _anim.SetTrigger("changeState"); 
    }

    public void DontInteract()
    {        
    }
}
