using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour, IInteractable
{
    public void DontInteract()
    {
        throw new System.NotImplementedException();
    }

    public void Interact()
    {
        Gamedata._hasRedKey = true;        
        Destroy(gameObject);
    }
}
