using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoor : MonoBehaviour, IInteractable
{
    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    public void Interact()
    {
        if (Gamedata._hasRedKey == true)
        {
            _anim.SetTrigger("changeState");
        }
        else
        {
            Debug.Log("Find the key?");
        }
    }

    public void DontInteract()
    {
        throw new System.NotImplementedException();
    }


}
