using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckDoor : MonoBehaviour, IInteractable
{
    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    public void Interact()
    {
        _anim.SetTrigger("isOpen");
    }

    public void DontInteract()
    {        
    }
}
