using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class USBCable : MonoBehaviour, IInteractable
{
    public void DontInteract()
    {
        throw new System.NotImplementedException();
    }

    public void Interact()
    {
        Gamedata._hasCable = true;
        Debug.Log("found the cable");
        Destroy(gameObject);
    }
}
