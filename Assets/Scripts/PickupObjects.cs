using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObjects : MonoBehaviour                  //Do not use this script
{
    [SerializeField]
    private Transform _hands;

    private GameObject _heldObject;
    private Rigidbody _objectRB;

    [Header("Physics Parameters")]
    [SerializeField]
    private float _pickupRange = 5f;

    [SerializeField]
    private float _pickupForce = 100f;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_heldObject == null)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, _pickupRange))
                {
                    //Pick
                    PickupObject(hit.transform.gameObject);
                }
            }
            else
            {
                DropObject();
            }
        }
        if (_heldObject != null)
        {
            MoveObject();
        }

        void PickupObject(GameObject _object)
        {
            if (_object.GetComponent<Rigidbody>())
            {
                _objectRB = _object.GetComponent<Rigidbody>();
                _objectRB.useGravity = false;
                _objectRB.drag = 10f;
                _objectRB.constraints = RigidbodyConstraints.FreezeRotation;

                _objectRB.transform.parent = _hands;
                _heldObject = _object;
            }
        }

        void DropObject()
        {
            _objectRB.useGravity = true;
            _objectRB.drag = 1f;
            _objectRB.constraints = RigidbodyConstraints.None;

            _objectRB.transform.parent = null;
            _heldObject = null;
        }

        void MoveObject()
        {
            if(Vector3.Distance(_heldObject.transform.position, _hands.position) > 0.1f)
            {
                Vector3 move = (_hands.position - _heldObject.transform.position);
                _objectRB.AddForce(move * _pickupForce);
            }
        }
    }
}


