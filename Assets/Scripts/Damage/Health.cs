using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Slider healthBar;

    public int fullH = 100;
    private int _health;

    private WaitForSeconds RegenTick = new WaitForSeconds (0.1f);
    private Coroutine regen;

    public static Health instance;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        _health = fullH;
        healthBar.maxValue = fullH;
        healthBar.value = fullH;
    }
     public void usehealth(int amt)
     {
         if(_health - amt >= 0)
         {
             _health -= amt;
             healthBar.value = _health;

            if(regen != null)
            StopCoroutine(regen);

            regen = StartCoroutine(Regenhealth());
         }
         else if(_health == 0)
         {
             Debug.Log("dead");
         }
     }

     private void OnTriggerStay(Collider other)
     {
         if(other.tag == "Danger")
         {
             usehealth(1);
         }
     }

     private IEnumerator Regenhealth()
     {
         yield return new WaitForSeconds(1);

         while(_health < fullH)
         {
             _health += fullH / 100;
             healthBar.value = _health;
             yield return RegenTick; 
         }
         regen = null;
     }
}
