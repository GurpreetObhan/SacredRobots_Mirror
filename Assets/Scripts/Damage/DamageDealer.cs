using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField]
    private float _maxHealth = 100f;

    [SerializeField]
    private float _health = 100f;

    private void Start()
    {
        _health = _maxHealth;
    }

    IEnumerator DamageOverTime(float _dealDamage, float _time)
    {
        float doDamage = 0f;
        float duration = _dealDamage / _time;
        while (doDamage < _dealDamage)
        {
            _health -= duration;
            Debug.Log(_maxHealth.ToString());
            doDamage += duration;
            yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator HealOverTime(float _heal, float _time)
    {
        float increaseHealth = 0f;
        float duration = _heal / _time;
        while (increaseHealth < _heal)
        {
            _health += duration;
            Debug.Log(_maxHealth.ToString());
            increaseHealth += duration;
            yield return new WaitForSeconds(2f);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Danger")
        {
            StartCoroutine(DamageOverTime(5f, 20f));
        }
        if (_health == 0f)
        {
            StopAllCoroutines();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Danger")
        {
            StartCoroutine(HealOverTime(1f, 20f));
        }
    }
}
