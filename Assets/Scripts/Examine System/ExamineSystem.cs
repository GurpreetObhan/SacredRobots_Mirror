using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExamineSystem : MonoBehaviour
{
    [SerializeField]
    private float _distance;

    [SerializeField]
    private Transform _examineView;

    [SerializeField]
    private float _rotSensitivity;

    Vector3 _originalPos;
    bool _isInteract;
    GameObject _inspected;
    private Camera _camera;

    [SerializeField]
    private FirstPersonAIO _playerScript;

    private void Start()
    {
        _isInteract = false;
        //_camera = GetComponent<Camera>();
    }

    private void Update()
    {
        //var ray = _camera.ScreenPointToRay(Input.mousePosition);
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, forward, out hit, _distance))
        {
            if (hit.transform.tag == "Examinable" && !_isInteract)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    _inspected = hit.transform.gameObject;
                    _originalPos = hit.transform.position;
                    _isInteract = true;

                    StartCoroutine(PickupObject());
                }
            }
        }

        if (_isInteract)
        {
            _inspected.transform.position = Vector3.Lerp(_inspected.transform.position, _examineView.position, 0.2f);
            _examineView.Rotate(new Vector3(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0f) * Time.deltaTime * _rotSensitivity);
        }
        else if(_inspected != null)
        {
            _inspected.transform.SetParent(null);
            _inspected.transform.position = Vector3.Lerp(_inspected.transform.position, _originalPos, 0.2f);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && _isInteract)
        {
            StartCoroutine(DropObject());
            _isInteract = false;
        }
    }

    IEnumerator PickupObject()
    {
        _playerScript.enabled = false;
        yield return new WaitForSeconds(0.2f);
        _inspected.transform.SetParent(_examineView);
    }

    IEnumerator DropObject()
    {
        _inspected.transform.rotation = Quaternion.identity;
        yield return new WaitForSeconds(0.2f);
        _playerScript.enabled = true;
    }

}
